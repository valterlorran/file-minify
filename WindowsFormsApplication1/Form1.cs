﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<String> files = new List<String>();
        int files_edited = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            DirectoryInfo di = new DirectoryInfo(fbd.SelectedPath);

            this.DirSearch(fbd.SelectedPath);
        }

        private List<String> DirSearch(string sDir)
        {
            try
            {
                files = this.getDiretorios(sDir);
                foreach (string f in files)
                {
                    string path = f;
                    if (Path.GetExtension(path) != ".js" && Path.GetExtension(path) != ".css") continue;
                
                }
                label1.Text = files_edited.ToString()+" files edited";
                files_edited = 0;
            }
            catch (System.Exception excpt)
            {
                MessageBox.Show(excpt.Message);
            }
            return files;
        }
        /*
         * Recursive methdo to get all files and folders inside the selected directory. 
         */
        private List<String> getDiretorios(string dir)
        {
            
            List<String> files = new List<String>();
            foreach (string f in Directory.GetFiles(dir))
            {
                string path = f;
                if ((js.Checked && Path.GetExtension(f) == ".js") || (css.Checked && Path.GetExtension(f) == ".css"))
                {
                    this.files_edited++;
                    var text = File.ReadAllText(path);
                    //regex to remove comments
                    text = Regex.Replace(text, @"//[^\n]*\n|/\*(.|[\r\n])*?\*/", "");
                    //remove the useless chars
                    text = text.Replace("\n", "");
                    text = text.Replace("\r", "");
                    text = text.Replace("\t", "");
                    text = text.Replace("  ", " ");
                    text = text.Replace("  ", " ");
                    text = text.Replace("   ", "");
                    File.WriteAllText(path, text);
                    text = null;
                }
                else
                {
                    continue;
                }
                //files.Add(f);
            }
            foreach (string d in Directory.GetDirectories(dir))
            {
                files.AddRange(getDiretorios(d));
            }
            return files;
        }
    }
}
